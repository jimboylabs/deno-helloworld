FROM denoland/deno:1.28.1

EXPOSE 3000

WORKDIR /app

COPY . . 

RUN deno cache main.ts

CMD  ["run", "--allow-net", "main.ts"]